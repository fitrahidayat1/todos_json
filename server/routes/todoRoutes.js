const express = require("express");

const {
  getAllTodos,
  getTodo,
  createTodo,
  updateTodo,
  deleteTodo,
} = require("../controllers/todoController.js");

const router = express.Router();

//Get All Todos
router.get("/todos", getAllTodos);

//Get single record by id
router.get("/todo/:id", getTodo);

//Create User
router.post("/todo", createTodo);

//Update todo
router.put("/todo/:id", updateTodo);

//Delete Todo
router.delete("/todo/:id", deleteTodo);

module.exports = router;
