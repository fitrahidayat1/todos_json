const faker = require("faker");
const TodoList = require("../models/Todos");

//Class Todolist for read, add, save, update and delete JSON file
const todolist = new TodoList();

//Create random id using Faker
const uuid = faker.datatype.uuid();

//Get All Todos
const getAllTodos = (req, res) => {
  res.send({
    statusCode: 200,
    statusMessage: "success",
    data: todolist.readAll(),
  });
};

//Get single Todo
const getTodo = (req, res) => {
  const id = req.params.id;
  const todo = todolist.readSingle(id);
  if (todo) {
    res.send({
      statusCode: 200,
      statusText: "success",
      data: todo,
    });
  } else {
    res.send(`ID: ${req.params.id} tidak ditemukan`);
  }
};

//Create Todo
const createTodo = (req, res) => {
  todolist.add({
    id: uuid,
    ...req.body,
  });
  todolist.save();
  res.send({
    statusCode: 200,
    statusText: "success",
    statusMessage: `Todo "${req.body.title}" berhasil ditambahkan`,
    data: todolist.readAll(),
  });
};

//Update Todo
const updateTodo = (req, res) => {
  const id = req.params.id;
  const todo = todolist.readSingle(id);
  if (todo) {
    const title = req.body.title;
    const isCompleted = req.body.isCompleted;

    todolist.update(id, title, isCompleted);
    res.send({
      statusCode: 200,
      statusText: "success",
      statusMessage: `Todo dengan ${id} berhasil di update`,
      data: todo,
    });
  } else {
    res.send(`Todo dengan ID: ${id} tidak ditemukan`);
  }
};

//Delete Todo
const deleteTodo = (req, res) => {
  const id = req.params.id;
  const todo = todolist.readSingle(id);
  if (todo) {
    todolist.delete(id);
    res.send({
      statusCode: 200,
      statusText: "success",
      statusMessage: `Todo dengan ID ${id} berhasil di hapus`,
      data: todolist.readAll(),
    });
  } else {
    res.send(`Todo dengan ID ${id} tidak ditemukan`);
  }
};

module.exports = { getAllTodos, getTodo, createTodo, updateTodo, deleteTodo };
