# API List

| Method | EndPoint  | Description     |
| ------ | --------- | --------------- |
| GET    | /todos    | List of todos   |
| GET    | /todo{id} | View a todo     |
| POST   | /todo     | Create new todo |
| PUT    | /todo{id} | Update a todo   |
| DELETE | /todo{id} | Delete a todo   |

---

# How to run

## Server

```
$ cd server
$ npm i
$ npm start
```

## Client

```
$ cd client
$ npm i
$ npm start
```
