const fs = require("fs");

class TodoList {
  constructor() {
    const data = fs.readFileSync("./todos.json", "utf-8");
    this.fileJSON = JSON.parse(data);
    this.todo = [];
  }

  readAll() {
    return this.fileJSON;
  }

  get allTodo() {
    return this.readAll();
  }

  readSingle(id) {
    this.todo = this.fileJSON.find((item) => item.id == id);
    return this.todo;
  }

  add(item) {
    this.fileJSON.push(item);
  }

  save() {
    fs.writeFile(
      "./todos.json",
      JSON.stringify(this.fileJSON, null, 2),
      (err) => {
        if (err) {
          console.log("Data gagal disimpan");
        }
        console.log("Data berhasil disimpan");
      }
    );
  }

  update(id, title, isCompleted) {
    const todo = this.fileJSON.find((item) => item.id == id);
    todo.title = title;
    todo.isCompleted = isCompleted;
    this.save();
  }

  delete(id) {
    this.fileJSON = this.fileJSON.filter((item) => item.id !== id);
    this.save();
  }
}

module.exports = TodoList;
