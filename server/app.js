const express = require("express");
const bodyParser = require("body-parser");

const todosRoutes = require("./routes/todoRoutes.js");

const app = express();
const port = 8080;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/api/v1", todosRoutes);

app.listen(port, (error) => {
  if (error) {
    console.log("Error listening port");
  } else {
    console.log(`Listening on port ${port}`);
  }
});
